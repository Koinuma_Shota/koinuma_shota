//投稿用のServiceクラス

package com.example.demo.Service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Message;
import com.example.demo.Repository.MessageRepository;

@Service
public class MessageService {
	@Autowired
	MessageRepository messageRepository;
	public List<Message> findAllMessage() {
		return messageRepository.findAll(Sort.by(Sort.Direction.DESC,"id"));
	}


	//絞り込み機能(日付）
	public List<Message> serchMessage(Date start, Date end) {
		return messageRepository.findBycreatedDateBetween(start, end);
	}

	//投稿のレコード追加
	public void saveMessage(Message message) {
		messageRepository.save(message);
	}

	//投稿のレコードの削除
	public void deleteMessage(Integer id) {
		messageRepository.deleteById(id); //"deleteById"でEntityに対する削除操作
	}

	//絞り込み機能(カテゴリー）
	public List<Message> serchCategory(String serchCategory) {
		return messageRepository.findByCategoryContaining(serchCategory);
	}


}

