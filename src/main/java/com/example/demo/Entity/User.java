//ユーザー用のエンティティ
package com.example.demo.Entity;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.example.demo.Validation.UserValid;
import com.example.demo.Validation.ValidLength;
import com.example.demo.Validation.ValidNull;


@SessionScope
@Component
@Entity
@Table(name = "users")
public class User {


	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@NotBlank(message = "アカウントが入力されていません", groups = {ValidNull.class, UserValid.class})
	@Length(min=6,max=50,message="アカウント名は25文字以下で入力して下さい", groups = ValidLength.class)
	@Column(name = "account")
	private String account;

	@NotBlank(message = "パスワードが入力されていません", groups = {ValidNull.class, UserValid.class})
	@Length(min=6,max=50,message="パスワードは20文字以下で入力して下さい", groups = ValidLength.class)
	@Column(name = "password")
	private String password;

	@NotBlank(message = "ユーザー名を入力してください", groups = UserValid.class)
	@Length(min=1,max=50,message="ユーザー名は10文字以下で入力して下さい", groups = ValidLength.class)
	@Column(name = "name")
	private String name;

	@Column(name = "branch_id")
	private int branchId;

	@Column(name = "department_id")
	private int departmentId;

	@Column(name = "created_date", insertable=false, updatable=false)
	private Date createdDate;

	@Column(name = "updated_date", insertable=false)
	private Date updatedDate;


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getAccount() {
		return account;
	}

	public void setAccount(String account) {
		this.account = account;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getBranchId() {
		return branchId;
	}

	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}

	public int getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(int departmentId) {
		this.departmentId = departmentId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}



}
