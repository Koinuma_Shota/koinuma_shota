//トップ画面のControllerクラス
package com.example.demo.Controller;

	import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Entity.Comment;
import com.example.demo.Entity.Message;
import com.example.demo.Entity.User;
import com.example.demo.Service.CommentService;
import com.example.demo.Service.MessageService;
import com.example.demo.Service.UserService;


	@Controller //Controllerである事を示すためのアノテーション
	public class TopController {
		@Autowired
		 UserService userService;
		@Autowired
		MessageService messageService;
		@Autowired
		CommentService commentService;


		// 投稿の表示処理
		@GetMapping
		public ModelAndView top(HttpServletRequest request, HttpServletResponse response) {
			User user = (User) request.getSession().getAttribute("loginUser");

			ModelAndView mav = new ModelAndView();
			// 投稿を全件取得(Service→Repositoryへと処理が続き、各値がReport型のリスト"contentDataへ格納される)
			List<Message> messageData = messageService.findAllMessage();
			List<Comment> commentData = commentService.findAllComment();
			// 画面遷移先の指定
			mav.setViewName("/top");
			// 投稿データオブジェクトを保管
			mav.addObject("messages", messageData);
			mav.addObject("comments",commentData);
			mav.addObject("loginUser",user);
			return mav;
		}
		//投稿の絞り込み機能(日付)
		@GetMapping("/serch")

		public ModelAndView Serch(@RequestParam(name="startDate",required = false) String startDate,
				                                  @RequestParam(name="endDate",required = false) String endDate,
				                                  HttpServletRequest request, HttpServletResponse response)
				                                throws ParseException {

			User user = (User) request.getSession().getAttribute("loginUser");

			SimpleDateFormat sdFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			Date now = new Date();
			String defaultEnd = sdFormat.format(now);
			String defaultStart = "2020-01-01 00:00:00";

			if(!StringUtils.isEmpty(startDate)){
				startDate += " 00:00:00";
			}else {
				startDate = defaultStart;
			}
			if(!StringUtils.isEmpty(endDate)) {
				endDate += " 23:59:59";
			}else {
				endDate = defaultEnd;
			}
			//String型からDate型へ型変換する
			Date start = sdFormat.parse(startDate);
			Date end = sdFormat.parse(endDate);

			ModelAndView mav = new ModelAndView();
			List<Message> serchDate =messageService.serchMessage(start,end);
			List<Comment> commentData = commentService.findAllComment();
			mav.addObject("messages", serchDate);
			mav.addObject("comments",commentData);
			mav.addObject("loginUser",user);
			mav.setViewName("/top");
			return mav;
		}

		//投稿の絞り込み機能
		@GetMapping("/serchCategory")
		public ModelAndView serchCategory(@RequestParam(name = "serchCategory")String serchCategory,
																HttpServletRequest request, HttpServletResponse response) {
			User user = (User) request.getSession().getAttribute("loginUser");
			ModelAndView mav = new ModelAndView();
			List<Message> serchDate =messageService.serchCategory(serchCategory);
			List<Comment> commentData = commentService.findAllComment();
			mav.addObject("messages", serchDate);
			mav.addObject("comments",commentData);
			mav.addObject("loginUser",user);
			mav.setViewName("/top");
			return mav;

		}
}