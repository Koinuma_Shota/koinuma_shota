package com.example.demo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.User;
import com.example.demo.Repository.UserRepository;

@Service
public class UserService {
	@Autowired
	UserRepository userRepository;

	public List<User> selectAll() {
		return userRepository.findAll();
	}

	public void saveUser(User user) {
		userRepository.save(user);
	}

	public User findByUser(String account, String password) {
		User user = (User) userRepository.findByAccountAndPassword(account, password);
		return user;
	}

	public User findByAccount(String account) {
		User userAccount = (User) userRepository.findByAccount(account);
		return userAccount;
	}
}
