//支店用のServiceクラス
package com.example.demo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Branch;
import com.example.demo.Repository.BranchRepository;

@Service
public class BranchService {

	@Autowired
	BranchRepository branchRepository;

	public List<Branch> findAll() {
		return branchRepository.findAll();
	}

}
