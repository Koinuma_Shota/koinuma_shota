//コメント用のRepositoryクラス
package com.example.demo.Repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.Message;


@Repository //JpaRepositoryである事を示すアノテーション
public interface MessageRepository extends JpaRepository<Message, Integer> {
	public List<Message> findBycreatedDateBetween(Date start,Date end);
	public List<Message> findByCategoryContaining(String serchCategory);
	}
