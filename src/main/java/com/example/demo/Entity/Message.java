//投稿用のエンティティ
package com.example.demo.Entity;


import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.example.demo.Validation.ValidLength;
import com.example.demo.Validation.ValidNull;



@Entity
@Table(name = "messages")

public class Message {



	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;


	@NotBlank(message = "件名を入力して下さい",groups = ValidNull.class)  //空文字含めたバリデーション
	@Length(min=1,max=30,message="件名は30文字以下で入力して下さい", groups = ValidLength.class) //文字数バリデーション
	@Column(name = "title")
	private String title;

	@NotBlank(message = "本文を入力して下さい",groups = {ValidNull.class})
	@Length(min=1,max=1000,message="本文は1000文字以下で入力して下さい", groups = ValidLength.class)
	@Column(name = "text")
	private String text;

	@NotBlank(message = "カテゴリーを入力して下さい",groups = ValidNull.class)
	@Length(min=1,max=10,message="カテゴリーは10文字以下で入力して下さい", groups = ValidLength.class)
	@Column(name = "category")
	private String category;

	@Column(name = "user_id")
	private int userId;

	@Column(name ="created_date", insertable = false, updatable = false)
	private Date createdDate;

	@Column(name = "updated_date", insertable=false)
	private Date updatedDate;




	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}



}
