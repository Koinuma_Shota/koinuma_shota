//投稿に関するController
package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Entity.Message;
import com.example.demo.Entity.User;
import com.example.demo.Service.CommentService;
import com.example.demo.Service.MessageService;
import com.example.demo.Validation.ValidAll;

@Controller
public class MessageController {
	@Autowired
	MessageService messageService;
	@Autowired
	CommentService commentService;
	//投稿機能
	@GetMapping("/new")
	public ModelAndView newMessage(HttpServletRequest request, HttpServletResponse response) {
		User user = (User) request.getSession().getAttribute("loginUser");
		ModelAndView mav = new ModelAndView();
		// form⽤の空のentityを準備
		Message message = new Message();
		// 画⾯遷移先を指定
		mav.setViewName("/new");
		// 準備した空のentityを保管
		mav.addObject("formModel", message);
		mav.addObject("loginUser", user);
		return mav;
	}


	// 投稿処理
	@PostMapping("/newMessage")
	public ModelAndView addContent(@Validated(ValidAll.class) @ModelAttribute("formModel") Message message, BindingResult result, Model model,HttpServletRequest request, HttpServletResponse response){

		//エラーメッセージの表示
		List<String> errorList = new ArrayList<String>();
		if (result.hasErrors()) {
            for (ObjectError error : result.getAllErrors()) {
                errorList.add(error.getDefaultMessage());
            }
            model.addAttribute("validationError", errorList);
            return new ModelAndView("/new");
		}else {
			User user = (User) request.getSession().getAttribute("loginUser");
			message.setUserId(user.getId());
			// 投稿をテーブルに格納
			messageService.saveMessage(message);
			// rootへリダイレクト
			return new ModelAndView("redirect:/");
		}
	}

	//投稿削除機能
	@DeleteMapping("/deleteMessage/{id}")
	public ModelAndView deleteMessage(@PathVariable Integer id) {
		messageService.deleteMessage(id);
		return new ModelAndView("redirect:/");
	}
}
