package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.Comment;
import com.example.demo.Entity.User;
import com.example.demo.Service.CommentService;
import com.example.demo.Validation.ValidAll;

@Controller
public class CommentController {
	@Autowired
	CommentService commentService;
	//コメント機能
	@PostMapping("/addComment")
	public ModelAndView newComment(@Validated(ValidAll.class) @ModelAttribute("formModel")
															Comment comment, BindingResult result, Model model,
															HttpServletRequest request, HttpServletResponse response,RedirectAttributes attributes) {

		User user = (User) request.getSession().getAttribute("loginUser");
		ModelAndView mav = new ModelAndView();
		List<String> errorList = new ArrayList<String>();
		//エラーメッセージの表示
		if (result.hasErrors()) {
            for (ObjectError error : result.getAllErrors()) {
                errorList.add(error.getDefaultMessage());
            }
            attributes.addFlashAttribute("validationError", errorList);
            attributes.addFlashAttribute("loginUser", user);
            return new ModelAndView("redirect:/");
		}else {

			// 準備した空のentityを保管
			mav.addObject("formModel", comment);
			String text = comment.getText();
			Integer userId = comment.getUserId();
			Integer messageId = comment.getMessageId();
			// 投稿をテーブルに格納
			comment.setText(text);
			comment.setId(userId);
			comment.setMessageId(messageId);
			comment.setUpdatedDate(new Date());
			commentService.saveComment(comment);
		}
		return new ModelAndView("redirect:/");
	}
	//投稿削除機能
	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}
}
