package com.example.demo.Validation;

import javax.validation.GroupSequence;

@GroupSequence({ValidNull.class, ValidLength.class})
public interface ValidAll {

}
