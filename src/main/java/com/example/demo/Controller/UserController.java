package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.Branch;
import com.example.demo.Entity.Department;
import com.example.demo.Entity.User;
import com.example.demo.Service.BranchService;
import com.example.demo.Service.DepartmentService;
import com.example.demo.Service.UserService;
import com.example.demo.Validation.UserValidAll;

@Controller
public class UserController {

	@Autowired
	UserService userService;
	@Autowired
	BranchService branchService;
	@Autowired
	DepartmentService departmentService;
	@Autowired
	HttpSession session;
	@Autowired
	User user;

	//新規ユーザー登録画面
	@GetMapping("/newUser")
	public ModelAndView newUser(Model model, HttpServletRequest request, HttpServletResponse response, RedirectAttributes attributes) {
		ModelAndView mav = new ModelAndView();
		//空のuserを作成
		User user = new User();

		//支店、部署の情報をDBから持ってくる
		List<Branch> branchData = branchService.findAll();
		List<Department> departmentData = departmentService.findAll();
		//投稿データオブジェクトを保管
		mav.addObject("branchData", branchData);
		mav.addObject("departmentData", departmentData);
		mav.addObject("formModel", user);

		//ログインユーザーの情報を取得してバリデーション
		User loginUser = (User)request.getSession().getAttribute("loginUser");
		//ログインしている人が本社の総務人事部の社員かどうかを判定する　権限がありません
		List<String> errorList = new ArrayList<String>();

		//バリデーションの実施
		if (loginUser.getBranchId() == 1 && loginUser.getDepartmentId() == 1) {
			//画面遷移先を指定
			mav.setViewName("/newUser");
			return mav;
		} else {
			errorList.add("権限がありません");
			attributes.addFlashAttribute("loginUser", loginUser);
			attributes.addFlashAttribute("validationError", errorList);
            return new ModelAndView("redirect:/");
		}

	}

	@PostMapping("/addUser")
	public ModelAndView signUp(@RequestParam("checkPassword") String checkPassword, @Validated(UserValidAll.class) @ModelAttribute("formModel") User user,
								BindingResult userResult,HttpServletRequest request, HttpServletResponse response,
								RedirectAttributes attributes, Model model) {

		ModelAndView mav = new ModelAndView();

		//DBからアカウント情報を確認
		String account = user.getAccount();
		User signUpUser = userService.findByAccount(account);

		//入力されたパスワードを取得
		String password = user.getPassword();

		//エラーメッセージの入れ物を作成
		List<String> errorList = new ArrayList<String>();

		//支店、部署の情報をDBから持ってくる（バリデーション後再設定）
		List<Branch> branchData = branchService.findAll();
		List<Department> departmentData = departmentService.findAll();

		//バリデーションの実施
		if (userResult.hasErrors()) {
            for (ObjectError error : userResult.getAllErrors()) {
                errorList.add(error.getDefaultMessage());
            }
            mav.addObject("validationError", errorList);
		}

		if(signUpUser != null) {
			errorList.add("既に存在するアカウント名です");
			mav.addObject("validationError", errorList);
		}

		if (!password.equals(checkPassword)) {
			errorList.add("入力されたパスワードが間違っています");
			mav.addObject("validationError", errorList);
		}

		//以下、支店と部署の組み合わせ判定
		if(user.getBranchId() == 1 && user.getDepartmentId() == 3) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 1 && user.getDepartmentId() == 4) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 2 && user.getDepartmentId() == 1) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 2 && user.getDepartmentId() == 2) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 2 && user.getDepartmentId() == 5) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 3 && user.getDepartmentId() == 1) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 3 && user.getDepartmentId() == 2) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 3 && user.getDepartmentId() == 5) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 4 && user.getDepartmentId() == 1) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 4 && user.getDepartmentId() == 2) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 4 && user.getDepartmentId() == 5) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 5 && user.getDepartmentId() == 1) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 5 && user.getDepartmentId() == 2) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 5 && user.getDepartmentId() == 5) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 6 && user.getDepartmentId() == 1) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 6 && user.getDepartmentId() == 2) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		if(user.getBranchId() == 6 && user.getDepartmentId() == 5) {
			errorList.add("支店と部署の組み合わせが不正です");
			mav.addObject("validationError", errorList);
		}

		//これが支店部署の比較より後にくる
		if (!errorList.isEmpty()) {
			//支店、部署の情報を保管
			mav.addObject("branchData", branchData);
			mav.addObject("departmentData", departmentData);
			mav.setViewName("/newUser");
			return mav;
		}

		//ここではサービスに保管した値を渡す
		userService.saveUser(user);
		return new ModelAndView("redirect:/");

	}

}