//投稿削除ダイアログ
$(document).ready(function(){
	$('#delete').on('click', function(){
		if(!confirm('この投稿を削除しますか？')){
			return false;
		} else {
			alert('削除しました');
		}
	});
});
	//コメント削除ダイアログ
	$(document).ready(function(){
		$('#deleteComment').on('click', function(){
			if(!confirm('このコメントを削除しますか？')){
				return false;
			} else {
				alert('削除しました');
			}
		});
	});