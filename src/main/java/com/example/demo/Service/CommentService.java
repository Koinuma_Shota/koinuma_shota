//コメント用のServiceクラス
package com.example.demo.Service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.Entity.Comment;
import com.example.demo.Repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;
	public List<Comment> findAllComment() {
		return commentRepository.findAll();
	}
	//コメントのレコード追加
	public void saveComment(Comment comment) {
		commentRepository.save(comment);
	}
	//コメントのレコードの削除
	public void deleteComment(Integer id) {
		commentRepository.deleteById(id); //"deleteById"でEntityに対する削除操作
	}


}
