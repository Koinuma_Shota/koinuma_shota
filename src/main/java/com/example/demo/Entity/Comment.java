//コメント用のエンティティ
package com.example.demo.Entity;






import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.hibernate.validator.constraints.Length;

import com.example.demo.Validation.ValidLength;
import com.example.demo.Validation.ValidNull;

@Entity
@Table(name = "comments")
public class Comment {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	@NotBlank(message = "コメントを入力して下さい",groups = ValidNull.class)  //空文字含めたバリデーション
	@Length(min=1,max=500,message="本文は500文字以下で入力して下さい", groups = ValidLength.class) //文字数バリデーション
	@Column(name = "text")
	private String text;

	@Column(name = "user_id")
	private int userId;

	@Column(name = "message_id")
	private int messageId;

	@Column(name ="created_date", insertable = false, updatable = false)
	private Date createdDate;

	@Column(name = "updated_date", insertable=false)
	private Date updatedDate;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public int getMessageId() {
		return messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public Date getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}




}
