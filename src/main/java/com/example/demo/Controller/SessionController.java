package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.Entity.User;
import com.example.demo.Service.UserService;
import com.example.demo.Validation.ValidAll;


@Controller
public class SessionController {

	@Autowired
	UserService userService;
	@Autowired
	HttpSession session;
	@Autowired
	User user;



	//ログイン画面表示
	@GetMapping("login")
    public ModelAndView getLogin(@ModelAttribute("formModel")User user, Model model) {
		ModelAndView mav = new ModelAndView();
		mav.setViewName("/login");
        return mav;
    }


	//ログイン成功時のトップへ遷移
	@PostMapping("login")
	public ModelAndView postLogin(@Validated(ValidAll.class) @ModelAttribute("formModel")User user, BindingResult result, Model model) {
		ModelAndView mav = new ModelAndView();


		String account = user.getAccount();
        String password = user.getPassword();

		User loginUser = userService.findByUser(account, password);

		List<String> errorList = new ArrayList<String>();
		if (result.hasErrors()) {
            for (ObjectError error : result.getAllErrors()) {
                errorList.add(error.getDefaultMessage());
            }
            model.addAttribute("validationError", errorList);
            mav.setViewName("/login");
            return mav;
        }else if(loginUser == null){
        	errorList.add("アカウントまたはパスワードが誤っています");
            model.addAttribute("validationError", errorList);
            return getLogin(user, model);
        }
        session.setAttribute("loginUser", loginUser);
        return new ModelAndView("redirect:/");
	}

	@GetMapping("logout")
	public ModelAndView logout() {
		ModelAndView mav = new ModelAndView();
        // セッションの無効化
		session.invalidate();
        mav.setViewName("/login");
        return mav;




    }




}
