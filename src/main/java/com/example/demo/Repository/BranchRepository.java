//支店用のRepositoryクラス
package com.example.demo.Repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.Branch;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer> {





}
